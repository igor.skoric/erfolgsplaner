package at.campus02.sp2_2017.erfolgsplaner;

import at.campus02.sp2_2017.erfolgsplaner.web.GoalController;
import at.campus02.sp2_2017.erfolgsplaner.web.UserController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ErfolgsplanerApplicationTests {

    @Autowired
    UserController userController;

    @Autowired
    GoalController goalController;

    @Test
    public void contextLoads() {
        assertThat(userController).isNotNull();
        assertThat(goalController).isNotNull();
    }
}
