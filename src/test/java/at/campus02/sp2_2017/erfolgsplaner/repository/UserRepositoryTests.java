package at.campus02.sp2_2017.erfolgsplaner.repository;

import at.campus02.sp2_2017.erfolgsplaner.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest // makes the test run in a in-memory database
public class UserRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindByUserName_thenReturnUser() {
        // create user by name
        User testUser = new User();
        testUser.setUsername("testuser");
        testUser.setPassword("testpassword");
        testUser.setPasswordConfirm("testpassword");

        entityManager.persistAndFlush(testUser);

        // request user back from repo by name
        User userFromRepo = userRepository.findByUsername(testUser.getUsername());

        // assert that both users have the same name
        assertThat(userFromRepo.getUsername()).isEqualTo(testUser.getUsername());
    }

    @Test(expected = ConstraintViolationException.class)
    public void whenCreateUserWithoutSettingPassword_thenException() {
        // create user by name
        User testUser = new User();
        testUser.setUsername("testuser");

        entityManager.persistAndFlush(testUser);
    }

    @Test(expected = ConstraintViolationException.class)
    public void whenCreateUserWithoutSettingUsername_thenException() {
        // create user by name
        User testUser = new User();
        testUser.setPassword("testpassword");
        testUser.setPasswordConfirm("testpassword");

        entityManager.persistAndFlush(testUser);
    }

    @Test(expected = ConstraintViolationException.class)
    public void whenCreateUserWithoutSettingAnything_thenException() {
        // create user by name
        User testUser = new User();

        entityManager.persistAndFlush(testUser);
    }

}
