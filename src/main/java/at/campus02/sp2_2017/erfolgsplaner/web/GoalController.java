package at.campus02.sp2_2017.erfolgsplaner.web;

import at.campus02.sp2_2017.erfolgsplaner.model.Goal;
import at.campus02.sp2_2017.erfolgsplaner.model.User;
import at.campus02.sp2_2017.erfolgsplaner.service.CalendarService;
import at.campus02.sp2_2017.erfolgsplaner.service.GoalService;
import at.campus02.sp2_2017.erfolgsplaner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;
import java.util.Map;
import java.util.Set;

@Controller
public class GoalController {

    @Autowired
    private GoalService goalService;

    @Autowired
    private UserService userService;

    @Autowired
    private CalendarService calendarService;

    private User getCurrentUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return userService.findByUsername(username);
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("title", "Übersicht");
        Set<Goal> goals = getCurrentUser().getGoals();
        model.addAttribute("goals", goals);
        return "home";
    }

    @RequestMapping(value = "/assist", method = RequestMethod.GET)
    public String assist(Model model) {
        model.addAttribute("title", "Ziel Assistent");
        model.addAttribute("formtitle", "Neue Ziele erstellen");
        model.addAttribute("assist", true);
        return "create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(Model model) {
        model.addAttribute("title", "Neues Ziel");
        model.addAttribute("formtitle", "Neues Ziel erstellen");
        return "create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@ModelAttribute("goalForm") Goal goalForm, @RequestParam("assist") String assist, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "create";
        }

        goalForm.setOwner(this.getCurrentUser());
        Date now = new Date(new java.util.Date().getTime());
        goalForm.setCreatedOn(now);
        goalService.create(goalForm);

        return assist.equals("true") ? "redirect:/assist" : "redirect:/";
    }

    @RequestMapping(value = "/done", method = RequestMethod.POST)
    public String done(@RequestParam("goal_id") String goalId) {
        Goal goal = goalService.findById(goalId);

        // only do this if current user is goal owner
        if (!goal.getOwner().equals(this.getCurrentUser())) {
            return "/";
        }

        Date now = new Date(new java.util.Date().getTime());
        goal.setLastDone(now);
        goal.setDoneTimes(goal.getDoneTimes() + 1);
        goalService.save(goal);
        return "redirect:/";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String delete(@RequestParam("goal_id") String goalId) {
        Goal goal = goalService.findById(goalId);

        // only do this if current user is goal owner
        if (!goal.getOwner().equals(this.getCurrentUser())) {
            return "/";
        }

        goalService.delete(goal);
        return "redirect:/";
    }
}
