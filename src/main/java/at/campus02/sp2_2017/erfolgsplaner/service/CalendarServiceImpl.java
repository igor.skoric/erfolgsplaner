package at.campus02.sp2_2017.erfolgsplaner.service;

import org.joda.time.*;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Calendar;

@Service
public class CalendarServiceImpl implements CalendarService {

    private boolean isSameByCalendarType(Date startDate, Date endDate, int calendarValueType) {
        return realUnitsBetween(startDate, endDate, calendarValueType) == 0;
    }

    private boolean isSameAsToday(Date date1, int calendarValueType) {
        Date now = new Date(new java.util.Date().getTime());
        return isSameByCalendarType(date1, now, calendarValueType);
    }

    private int possibleUnitsSince(Date date1, int calendarValueType) {
        Date now = new Date(new java.util.Date().getTime());
        return possibleUnitsBetween(date1, now, calendarValueType);
    }


    private int possibleUnitsBetween(Date date1, Date date2, int calendarValueType) {
        return realUnitsBetween(date1, date2, calendarValueType) + 1;
    }

    private int realUnitsBetween(Date date1, Date date2, int calendarValueType) {

        DateTime start = new DateTime(date1);
        DateTime end = new DateTime(date2);

        switch (calendarValueType) {
            case Calendar.DAY_OF_YEAR:
                return Days.daysBetween(start, end).getDays();
            case Calendar.WEEK_OF_YEAR:
                return Weeks.weeksBetween(start, end).getWeeks();
            default:
                return -1;
        }
    }

    @Override
    public boolean isSameDayAsToday(Date date1) {
        return isSameAsToday(date1, Calendar.DAY_OF_YEAR);
    }

    @Override
    public boolean isSameWeekAsToday(Date date1) {
        return isSameAsToday(date1, Calendar.WEEK_OF_YEAR);
    }

    @Override
    public int possibleWeeksSince(Date date1) {
        return possibleUnitsSince(date1, Calendar.WEEK_OF_YEAR);
    }

    @Override
    public int possibleDaysSince(Date date1) {
        return possibleUnitsSince(date1, Calendar.DAY_OF_YEAR);
    }
}
