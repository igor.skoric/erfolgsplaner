package at.campus02.sp2_2017.erfolgsplaner.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
