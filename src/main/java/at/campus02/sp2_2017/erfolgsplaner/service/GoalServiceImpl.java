package at.campus02.sp2_2017.erfolgsplaner.service;

import at.campus02.sp2_2017.erfolgsplaner.model.Goal;
import at.campus02.sp2_2017.erfolgsplaner.repository.GoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoalServiceImpl implements GoalService {

    @Autowired
    private
    GoalRepository goalRepository;

    @Override
    public void create(Goal goal) {
        goalRepository.save(goal);
    }

    @Override
    public void delete(Goal goal) {
        goalRepository.delete(goal);
    }

    @Override
    public void save(Goal goal) {
        goalRepository.save(goal);
    }

    @Override
    public Goal findById(Long id) {
        return goalRepository.findOne(id);
    }

    @Override
    public Goal findById(String goalId) {
        Long id = Long.parseLong(goalId);
        return this.findById(id);
    }
}
