package at.campus02.sp2_2017.erfolgsplaner.service;

import java.sql.Date;

public interface CalendarService {
    boolean isSameDayAsToday (Date date1);
    boolean isSameWeekAsToday (Date date1);
    int possibleWeeksSince(Date date1);
    int possibleDaysSince(Date date1);
}
