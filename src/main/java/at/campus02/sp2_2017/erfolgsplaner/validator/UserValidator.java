package at.campus02.sp2_2017.erfolgsplaner.validator;

import at.campus02.sp2_2017.erfolgsplaner.model.User;
import at.campus02.sp2_2017.erfolgsplaner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        String pflichtFeld = "Dieses Feld muss ausgefüllt werden!";

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty", pflichtFeld);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty", pflichtFeld);


        String username = user.getUsername();

        if (username.length() < 6 || username.length() > 32) {
            errors.rejectValue("username", null, "Die Länge muss zwischen 6 und 32 Zeichen liegen!");
        }

        User userFromQuery = userService.findByUsername(username);
        if (userFromQuery != null) {
            errors.rejectValue("username", null, "Dieser Benutername ist leider vergeben!");
        }

        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.rejectValue("password", null, "Die Länge muss zwischen 8 und 32 Zeichen liegen!");
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", null, "Passwort wurde nicht zwei mal identisch eingegeben!");
        }
    }
}
