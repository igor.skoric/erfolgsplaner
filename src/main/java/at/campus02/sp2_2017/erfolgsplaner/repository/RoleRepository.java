package at.campus02.sp2_2017.erfolgsplaner.repository;

import at.campus02.sp2_2017.erfolgsplaner.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
