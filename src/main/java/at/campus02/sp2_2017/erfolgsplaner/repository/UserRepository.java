package at.campus02.sp2_2017.erfolgsplaner.repository;

import at.campus02.sp2_2017.erfolgsplaner.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
